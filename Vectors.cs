using System;

namespace EpamHomeTasks
{
    class Vector3D
    {
        //constructor
        public Vector3D(int x, int y, int z)
        {
            this.X = x;
            this.Y = y;
            this.Z = z;
        }
        
        //PUBLIC PROPERTIES
        //public properties for coordinates
        public int X{ get; set; }
        
        public int Y{ get; set; }
        
        public int Z{ get; set; }
        
        //public property which return the length of a vector
        public double Length
        {
            get
            {
                return Math.Sqrt(Math.Pow(this.X, 2) + Math.Pow(this.Y, 2) + Math.Pow(this.Z, 2));
            }
        }
        
        //PUBLIC METHODS
        public static Vector3D Add(Vector3D v1, Vector3D v2)
        {
            Vector3D result = new Vector3D((v1.X + v2.X), (v1.Y + v2.Y), (v1.Z + v2.Z));    
            return result;
        }
        
        public static Vector3D Substract(Vector3D v1, Vector3D v2)
        {
            Vector3D result = new Vector3D((v1.X - v2.X), (v1.Y - v2.Y), (v1.Z - v2.Z));    
            return result;
        }
        
        public static double ScalarMultiply(Vector3D v1, Vector3D v2)
        {
             return v1.X * v2.X + v1.Y * v2.Y + v1.Z * v2.Z;
        }
        
        //vector multiplication of two vectors
        public static Vector3D VectorMultiply(Vector3D v1, Vector3D v2)
        {
            Vector3D result = new Vector3D(0, 0, 0);
            result.X = v1.Y * v2.Z - v1.Z * v2.Y;
            result.Y = v1.Z * v2.X - v1.X * v2.Z;
            result.Z = v1.X * v2.Y - v1.Y * v2.X;
            
            return result;
        }
        
        public static double MixedMultiply(Vector3D v1, Vector3D v2, Vector3D v3)
        {
            return v1 * Vector3D.VectorMultiply(v2, v3);
        }
        
        //method which calculate cos of an angle betwwen two vectors        
        public double Angle(Vector3D v)
        {
            double CosA = (this * v) / (this.Length * v.Length);
            return CosA;
        }
        
        //overloaded method Equals to compare two vectors
        public override bool Equals(object o)
        {
            if(o is Vector3D)
            {
                return this == (Vector3D)o;
            }
            else
            {
                return false;
            }
        }
        
        //overloaded method GetHashCode
        public override int GetHashCode()
        {
            return this.ToString().GetHashCode();
        }
    
        //ovverride mwthod which show all information about vector
        public override string ToString()
        {
            return String.Format("X = {0}, Y = {1}, Z = {2}, Length = {3:#.##}\n", this.X, this.Y, this.Z, this.Length);
        }
        
        
        //OVERLOADED OPERATORS
        //overloaded operator + to add two vectors
        public static Vector3D operator +(Vector3D v1, Vector3D v2)
        {
            return Vector3D.Add(v1, v2);
        }
        
        //overloaded operator - to substract one vector from another
        public static Vector3D operator -(Vector3D v1, Vector3D v2)
        {
            return Vector3D.Substract(v1, v2);
        }
        
        //overloaded operator * to get scalar multiplication of twoo vectors
        public static double operator *(Vector3D v1, Vector3D v2)
        {
            return Vector3D.ScalarMultiply(v1, v2);
        }

        //oerloaded operator == to compare two vectors
        public static bool operator ==(Vector3D v1, Vector3D v2)
        {
            return ((v1.X == v2.X) && (v1.Y == v2.Y) && (v1.Z == v2.Z) && (v1.Length == v2.Length));
        }
        
        public static bool operator !=(Vector3D v1, Vector3D v2)
        {
            return ((v1.X != v2.X) || (v1.Y != v2.Y) || (v1.Z != v2.Z) || (v1.Length != v2.Length));
        }      
    }
    
    class Program
    {
        static void Main()
        {
            Random rand = new Random();
            
            Vector3D first_vector = new Vector3D(rand.Next(1, 10), rand.Next(1, 10), rand.Next(1, 10));
            Console.WriteLine("first_vector: {0}", first_vector);
            
            Vector3D second_vector = new Vector3D(rand.Next(1, 10), rand.Next(1, 10), rand.Next(1, 10));
            Console.WriteLine("second_vector: {0}", second_vector);
            
            Console.ForegroundColor = ConsoleColor.Yellow;
            Console.WriteLine("ADDITION:");
            Console.WriteLine("first_vector + second_vector = {0}", first_vector + second_vector);  
            Console.WriteLine("Vector3D.Add(first_vector, second_vector) = {0}", Vector3D.Add(first_vector, second_vector));
            
            Console.ForegroundColor = ConsoleColor.Red;
            Console.WriteLine("SUBSTRACTION:");
            Console.WriteLine("first_vector - second_vector = {0}", first_vector - second_vector);
            Console.WriteLine("Vector3D.Substract(first_vector, second_vector) = {0}", Vector3D.Substract(first_vector, second_vector));
            
            Console.ForegroundColor = ConsoleColor.Green;
            Console.WriteLine("MULTIPLICATION:");
            Console.WriteLine("Scalar multiplication: first_vector * second_vector = {0}", first_vector * second_vector);
            Console.WriteLine("Scalar multiplication: Vector3D.ScalarMultiply(first_vector, second_vector) = {0}", Vector3D.ScalarMultiply(first_vector, second_vector));           
            Console.WriteLine("Vector multiplication: Vector3D.VectorMultiply(first_vector, second_vector) = {0}", Vector3D.VectorMultiply(first_vector, second_vector));
            Console.WriteLine("Mixed multiplication: Vector3D.MixedMultiply(first_vector, second_vector, third_vector) = {0}\n", Vector3D.MixedMultiply(first_vector, second_vector, new Vector3D(5, 1, 3)));
            
            Console.ForegroundColor = ConsoleColor.Cyan;
            Console.WriteLine("ANGLE:");
            Console.WriteLine("Cos of angle between first_vector and second_vector = {0:0.##}\n", first_vector.Angle(second_vector));
                  
            Console.ForegroundColor = ConsoleColor.Blue;
            Console.WriteLine("COMPARISON:");
            Console.WriteLine("first_vector.Equals(second_vector) = {0}", first_vector.Equals(second_vector));       
            Console.WriteLine("first_vector.Equals(first_vector) = {0}", first_vector.Equals(first_vector));       
            Console.WriteLine("first_vector == second_vector = {0}", first_vector == second_vector);        
            Console.WriteLine("first_vector != second_vector = {0}", first_vector != second_vector);         
            Console.WriteLine("first_vector == first_vector = {0}", first_vector == first_vector);           
            Console.WriteLine("first_vector != first_vector = {0}", first_vector != first_vector);
            Console.WriteLine("first_vector.GetHashCode() = {0}, second_vector.GetHashCode() = {1}", first_vector.GetHashCode(), second_vector.GetHashCode());
            
            Console.ForegroundColor = ConsoleColor.Gray;
        }
    }
}